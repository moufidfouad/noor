<?php

namespace App\Controller\Web;


use Twig\Environment;
use App\Entity\Produit;
use App\Repository\PostRepository;
use App\Repository\ProduitRepository;
use App\Repository\TemoignageRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController
{
    /** @var Environment $engine */
    private $engine;
    /** @var ProduitRepository $produitRepository */
    private $produitRepository;
    /** @var TemoignageRepository $temoignageRepository */
    private $temoignageRepository;
    /** @var PostRepository $postRepository */
    private $postRepository;

    public function __construct(
        Environment $engine,
        ProduitRepository $produitRepository,
        TemoignageRepository $temoignageRepository,
        PostRepository $postRepository
    ){
        $this->engine = $engine;
        $this->produitRepository = $produitRepository;
        $this->temoignageRepository = $temoignageRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * @Route("", name="web_index")
     */
    public function index()
    {
        $produits = $this->produitRepository->findLast(3)->getQuery()->getResult();
        $temoignages = $this->temoignageRepository->findLast(15)->getQuery()->getResult();
        $posts = $this->postRepository->findLast(3)->getQuery()->getResult();

        return new Response($this->engine->render('front/home/page.html.twig',[
            'produits' => $produits,
            'temoignages' => $temoignages,
            'posts' => $posts
        ]));
    }

    /**
     * @Route("/produit/{slug}", name="web_produit_show")
     */
    public function show(Produit $produit)
    {
        dd($produit);
    }
}