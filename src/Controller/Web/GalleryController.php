<?php

namespace App\Controller\Web;

use Twig\Environment;
use App\Repository\GalleryRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GalleryController
{
    /** @var Environment $engine */
    private $engine;
    /** @var PaginatorInterface $paginator */
    private $paginator;
    /** @var GalleryRepository $galleryRepository */
    private $galleryRepository;
    
    public function __construct(
        Environment $engine,
        PaginatorInterface $paginator,
        GalleryRepository $galleryRepository
    ){
        $this->engine = $engine;
        $this->paginator = $paginator;
        $this->galleryRepository = $galleryRepository;
    }
    
    /**
     * @Route("/gallery", name="web_galleries")
     */
    public function galleries(Request $request)
    {
        $query = $this->galleryRepository->createQueryBuilder('gallery');
        $pager = $this->paginator->paginate($query,$request->query->getInt('page',1),$request->query->getInt('limit',10));
       return new Response($this->engine->render('front/gallery/page.html.twig',[
           'pager' => $pager
       ]));
    }    
}