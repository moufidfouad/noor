<?php

namespace App\Controller\Web;

use Twig\Environment;
use App\Entity\Contact;
use App\Form\Type\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormFactoryInterface;

class ContactController
{
    /** @var Environment $engine */
    private $engine;
    /** @var FormFactoryInterface $form */
    private $form;
	
    public function __construct(
        Environment $engine,
		FormFactoryInterface $form
    ){
        $this->engine = $engine;
		$this->form = $form;
    }
    /**
     * @Route("/contact", name="web_contact")
     */
    public function contact(Request $request)
    {
        $form = $this->form->create(ContactType::class,new Contact(),[
            'method' => 'POST',
            'action' => $request->get('_route')
        ]);
		$form->handleRequest($request);
        return new Response($this->engine->render('front/contact/page.html.twig'));
    }    
}