<?php

namespace App\Controller\Web;

use Twig\Environment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutController
{
    /** @var Environment $engine */
    private $engine;
	
    public function __construct(
        Environment $engine
    ){
        $this->engine = $engine;
    }
	
    /**
     * @Route("/about", name="web_about")
     */
    public function about()
    {
       return new Response($this->engine->render('front/about/page.html.twig'));
    }    
}