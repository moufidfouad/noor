<?php

namespace App\Controller\Web\Admin;

use App\Entity\About;
use App\Entity\Post;
use App\Entity\Produit;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use Symfony\Component\Security\Core\User\UserInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="web_dashboard")
     */    
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<b>NOOR</b>')
            ->setFaviconPath('img/favicon.png')
        ;
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        return UserMenu::new()
            ->displayUserName()
            ->displayUserAvatar(false)
            ->setName($user instanceof User ? (string) $user->getFullname() : null)
            ->setAvatarUrl(null)
            ->setMenuItems([
                MenuItem::linkToLogout('security.profile.logout','fa fa-fw fa-lock')
            ])
        ;
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linktoDashboard('menu.homepage', 'fa fa-home'),

            MenuItem::subMenu('entity.post.name','fa fa-copy')->setSubItems([
                MenuItem::linkToCrud('entity.post.list.children.list','fa fa-th-list',Post::class)
                    ->setQueryParameter('crudAction','index'),
                MenuItem::linkToCrud('entity.post.list.children.create','fa fa-plus',Post::class)
                    ->setQueryParameter('crudAction','new')
            ]),

            MenuItem::subMenu('entity.produit.name','fa fa-copy')->setSubItems([
                MenuItem::linkToCrud('entity.produit.list.children.list','fa fa-th-list',Produit::class)
                    ->setQueryParameter('crudAction','index'),
                MenuItem::linkToCrud('entity.produit.list.children.create','fa fa-plus',Produit::class)
                    ->setQueryParameter('crudAction','new')
            ]),

            MenuItem::subMenu('entity.about.name','fa fa-copy')->setSubItems([
                MenuItem::linkToCrud('entity.about.list.children.list','fa fa-th-list',About::class)
                    ->setQueryParameter('crudAction','index'),
                MenuItem::linkToCrud('entity.about.list.children.create','fa fa-plus',About::class)
                    ->setQueryParameter('crudAction','new')
            ]),
        ];
    }
}