<?php

namespace App\Controller\Web\Admin\Crud;

use App\Entity\About;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class AboutCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return About::class;
    } 
    
    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setSearchFields(null)
            ->showEntityActionsAsDropdown(true)
            ->setPageTitle(Crud::PAGE_INDEX, 'entity.about.list.label')
            ->setPageTitle(Crud::PAGE_NEW, 'entity.about.form.actions.create')
            ->setPageTitle(Crud::PAGE_EDIT, 'entity.about.form.actions.edit')
            ->setPageTitle(Crud::PAGE_DETAIL, 'entity.about.form.actions.show')
        ;
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions
            //->disable(Action::DETAIL)
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
        ;
        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('adresse','entity.about.form.fields.adresse'),

            TextField::new('telephone','entity.about.form.fields.telephone'),

            EmailField::new('email','entity.about.form.fields.email'),

            UrlField::new('facebook','entity.about.form.fields.facebook')
                ->onlyOnForms(),

            UrlField::new('twitter','entity.about.form.fields.twitter')
                ->onlyOnForms(),

            UrlField::new('instagram','entity.about.form.fields.instagram')
                ->onlyOnForms(),

            TextareaField::new('contenu','entity.about.form.fields.contenu'),
        ];
    }     
}