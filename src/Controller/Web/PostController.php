<?php

namespace App\Controller\Web;

use Twig\Environment;
use App\Repository\PostRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/post")
 */
class PostController
{
    /** @var Environment $engine */
    private $engine;
    /** @var PostRepository $postRepository */
    private $postRepository;
    public function __construct(
        Environment $engine,
        PostRepository $postRepository
    ){
        $this->engine = $engine;
        $this->postRepository = $postRepository;
    }

    /**
     * @Route("", name="web_posts")
     */
    public function posts()
    {
       return new Response($this->engine->render('front/posts/page.html.twig'));
    } 

    /**
     * @Route("/show/{slug}", name="web_post_show")
     */
    public function show(Post $post)
    {
        dd($post);
    }
}