<?php

namespace App\Repository;

use Doctrine\ORM\QueryBuilder;

trait BaseRepositoryTrait
{
    private function getLast(QueryBuilder $qb,int $value,string $orderField = 'createdAt',string $orderType = 'DESC')
    {
        $qb->setMaxResults($value);
        if(!empty($order)){
            $qb->orderBy(sprintf('%s.%s',$qb->getRootAliases()[0],$orderField),$orderType);
        }

        return $qb;
    }
}