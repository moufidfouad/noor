<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PostRepository;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 * @Vich\Uploadable()
 */
class Post extends Gallery
{
    /**
     * @Vich\UploadableField(mapping="post", fileNameProperty="image")
     * 
     * @var File|null
     */
    protected $file;
}
