<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\Traits\TimestampableTrait;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"ADMIN" = "Admin"})
 * @ORM\Table(name="`user`")
 */
abstract class User implements UserInterface
{
    use TimestampableTrait;

    const ROLE_DEFAULT = 'ROLE_USER';
    const ROLE_CLIENT = 'ROLE_CLIENT';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $password;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $enabled;

    /**
     * @ORM\Column(type="array")
     */
    protected $roles = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $activation_token;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $reset_token;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    protected $email;

    /**
     * @var string|null $plain_password
     */
     protected $plain_password;

    public function __construct()
    {
        $this->enabled = true;
        $this->addRole(static::ROLE_DEFAULT);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        foreach($roles as $role){
            $this->addRole($role);
        }

        return $this;
    }

    public function getActivationToken(): ?string
    {
        return $this->activation_token;
    }

    public function setActivationToken(?string $activation_token): self
    {
        $this->activation_token = $activation_token;

        return $this;
    }

    public function getResetToken(): ?string
    {
        return $this->reset_token;
    }

    public function setResetToken(?string $reset_token): self
    {
        $this->reset_token = $reset_token;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function addRole(string $role)
    {
        if(substr($role, 0, 5) !== 'ROLE_') {
            throw new \InvalidArgumentException("Chaque role doit commencer par 'ROLE_'");
        }

        if(!in_array($role,$this->roles,true)){
            array_push($this->roles,$role);
            return true;
        }
        return false;
    }

    public function hasRole($role)
    {
        if(is_string($role)){
            return in_array(strtoupper($role),$this->roles);
        }
        
        if(is_array($role)){
            foreach($role as $v){
                if(in_array(strtoupper($v),$this->roles)){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plain_password;
    }

    public function setPlainPassword(string $plain_password): self
    {
        $this->plain_password = $plain_password;

        return $this;
    }

    abstract function getFullname();
}
