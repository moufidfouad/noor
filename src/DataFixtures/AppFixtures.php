<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Post;
use App\Entity\About;
use App\Entity\Produit;
use App\Entity\Temoignage;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for($i = 0; $i < 3; $i++){
            $produit = (new Produit())
                ->setTitre($faker->sentences(random_int(1,2),true))
                ->setPrix($faker->randomNumber(4,true))
                ->setContenu($faker->paragraphs(random_int(1,3),true))
                ->setImage(sprintf('image_%d.jpeg',$i+1))
            ;
            $manager->persist($produit);
        }


        $about = (new About())
            ->setContenu($faker->paragraphs(5,true))
            ->setEmail($faker->email())
            ->setTelephone($faker->phoneNumber())
            ->setAdresse($faker->address())
        ;
        $manager->persist($about);


        for($i = 0; $i < 100; $i++){
            $temoignage = (new Temoignage())
                ->setAuteur($faker->name())
                ->setContenu($faker->paragraphs(random_int(2,10),true))
            ;
            $manager->persist($temoignage);
        }

        for($i = 0; $i < 20; $i++){
            $post = (new Post())
                ->setTitre($faker->sentences(random_int(1,2),true))
                ->setContenu($faker->paragraphs(random_int(1,3),true))
                ->setImage($faker->randomElement(['image_1.jpeg','image_2.jpeg','image_3.jpeg']))
            ;
            $manager->persist($post);
        }

        $manager->flush();
    }
}
