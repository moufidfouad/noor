<?php

namespace App\Twig;

use LogicException;
use Twig\TwigFilter;
use App\Library\Tools;
use Twig\Extension\AbstractExtension;

class FilterExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('format_number', [$this, 'numberFormat'])
        ];
    }

    public function numberFormat($number,int $scale,$suffix = null)
    {
        if(!is_numeric($number)){
            throw new LogicException($number,implode(',',['int','float']));
        }

        return Tools::formatNumber($number,$scale,$suffix);
    }
}