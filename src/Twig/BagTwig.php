<?php

namespace App\Twig;

use App\Entity\About;
use App\Repository\AboutRepository;
use Doctrine\ORM\NoResultException;

class BagTwig
{
    /** @var AboutRepository $aboutRepository */
    private $aboutRepository;
    public function __construct(
        AboutRepository $aboutRepository
    )
    {
        $this->aboutRepository = $aboutRepository;
    }

    public function getAbout()
    {
        try{
            $about = $this->aboutRepository->findLast(1)->getQuery()->getSingleResult();
        }catch(NoResultException $e){
            $about = new About();
        }
        return $about;
    }
}