<?php

namespace App\Twig;

use Twig\Environment;
use Twig\TwigFunction;
use App\Entity\Contact;
use App\Form\Type\ContactType;
use Twig\Extension\AbstractExtension;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Form\FormFactoryInterface;

class FunctionExtension extends AbstractExtension
{
    /** @var Environment $engine */
    private $engine;
    /** @var FormFactoryInterface $form */
    private $form;
    /** @var RouterInterface $router */
    private $router;
    
    public function __construct(
        Environment $engine,
        FormFactoryInterface $form,
        RouterInterface $router
    ){
        $this->engine = $engine;
        $this->form = $form;
        $this->router = $router;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('contact_form',[$this, 'getContactForm'],['is_safe' => ['html']])
        ];
    }
    
    public function getContactForm(string $path,array $paramsPath = [])
    {
        $form = $this->form->create(ContactType::class,new Contact(),[
            'method' => 'POST',
            'action' => $this->router->generate($path,$paramsPath)
        ]);

        return $this->engine->render('extensions/contact_form.html.twig',[
            'form' => $form->createView()
        ]);
    }
}