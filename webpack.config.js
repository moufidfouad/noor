/**
 * Load Symfony Encore
 *
 * @type {Encore|Proxy}
 */
 let Encore = require('@symfony/webpack-encore');

 /**
  * Admin Assets Config
  */
 Encore
     // Set Output Path
    .setOutputPath('public/build/')
    .setPublicPath('/build')
 
 
    .addStyleEntry('css/app', './assets/styles/app.scss')
    .addEntry('js/app', './assets/scripts/app.js')

 
    .copyFiles({
        from: './assets/vendor/coffee',
        to: 'vendor/coffee/[path][name].[ext]'
    })

    .copyFiles({
        from: './node_modules/jquery-fancybox/source/js',
        to: 'vendor/coffee/js/jquery-fancybox/[path][name].[ext]'
    })

 
    // Configs
    .enableSassLoader()
    .enablePostCssLoader()
    .enableSourceMaps(!Encore.isProduction())
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableVersioning(false)
    .disableSingleRuntimeChunk();
 
 module.exports = Encore.getWebpackConfig();